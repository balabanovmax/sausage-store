#!/bin/bash
set +e
docker network create -d bridge sausage_network || true
docker pull gitlab.praktikum-services.ru:5050/std-021-011/sausage-store/sausage-frontend:latest
echo ${GITLAB_PASSWORD} | docker login -u ${GITLAB_USER} --password-stdin gitlab.praktikum-services.ru:5050
#docker pull ${GITLAB_REGISTRY}:latest
docker stop frontend || true
docker rm frontend || true
docker image prune --filter dangling=true -f  || true #Удаляем образы без тега
set -e
docker run -d --name frontend \
    --network=sausage_network \
    --restart always \
    --pull always \
    --publish 8080:80 \
    gitlab.praktikum-services.ru:5050/std-021-011/sausage-store/sausage-frontend:latest 
